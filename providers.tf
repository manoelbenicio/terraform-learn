terraform {
  required_providers {
    aws = {
      source = "hashicorp/aws"
      version = "5.8.0"
    }

    linode = {
      source = "linode/linode"
      version = "2.5.1"
    }
  }
}