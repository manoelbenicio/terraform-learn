# This file is maintained automatically by "terraform init".
# Manual edits may be lost in future updates.

provider "registry.terraform.io/hashicorp/aws" {
  version = "5.8.0"
  hashes = [
    "h1:LeGaxHyCnTpZdrhVCSJwDx91JcPWXlf4Rt4mnlF34Gs=",
    "zh:0974311d5e1becfdcbdae43d022d52689fdad32a4145659e56ac534bcb8cba02",
    "zh:100dc64a90fc0d36cf6e2882b4358fde17705edd8ab3c5f2c06d219c36b21565",
    "zh:467a86de8a7d77cde5c3386f9e82d7f1bf5972d1b3d177e797d1d9d2e87fd357",
    "zh:4ad1f8ef5c5522f81d271b93594a43a7666b3409ca201a1911cd950e489ef12b",
    "zh:540a50ab7061c6df2057ec9580890a9e86a687233120af738985fa84dde2a20a",
    "zh:6e7b73b770e92891da94751c3e0cff1e1b852f5121da8c4a689056833eeb7d94",
    "zh:879d42721e86331b05ff77bd219ca9a062485cdb2fa803d2dcf63084f25d484c",
    "zh:980563e615fbba127c02df6dc8872ce60f7137df45fdb8cd801cdcbae6cf192a",
    "zh:9b12af85486a96aedd8d7984b0ff811a4b42e3d88dad1a3fb4c0b580d04fa425",
    "zh:a6ad25c4d3edde466ea68731097aedad4b68278af0742fc1ab71d2c30491f92e",
    "zh:af8df9e06f576c11ce67ac2b675d0d8db4aac618fec95d27c10aa59436feebbf",
    "zh:b625ca7c4b99c6b3af34041b9773ccd9d80b0dde264c40b5d163a6abd73793af",
    "zh:c9e0ca6aa48ebaa0892ac438392c49052a86605f490950d5317855f35ab7d74a",
    "zh:dc500a03d3ed6b1fed3f118a55a7fb93bf172965ae6b2f25cc7f4a152e44edd7",
    "zh:e0438bf67d93a29f0d56f9a4544297155ca85c0f10626778d4c3aa68c7e93581",
  ]
}

provider "registry.terraform.io/linode/linode" {
  version     = "2.5.1"
  constraints = "2.5.1"
  hashes = [
    "h1:DbWWLX0o3K2IODHS4vQ5lIBwNy6yAL/HZFD4mPM8YCo=",
    "zh:0f0ce384dc923e8e5d6706f69e1cfaca864f107d866fedde49ec585afe90c394",
    "zh:13f5e5ec83eef48f91fe14f0843b323b75b6302d70d7f8870f2e5d4f59ccefef",
    "zh:230abf78ad02e17e042a410be3d0a1e80b332f00f51b20bdfb4d7ce2ff23a1d4",
    "zh:588dab68d0e284e3346d7143bf502015fc345ac8975ff919ef6a38bc58a59485",
    "zh:5d0aa8d211ef5ab1f3ef729e8f57e278cb0a64d58b3fede4fba0ac53bec7c06a",
    "zh:5fec12a6696ec13d77d6aa0d9868786eb7b627710357791e96dc1441b2062c19",
    "zh:7a6376ae0e4c8cdd4f81ac6e360ab515ba17af9b7e6d9e6aeaa509d6be911ba9",
    "zh:8d692a0baff9fa8784d4762ac053f05f7f1ac02cc138e94f8106f4f708354f12",
    "zh:9e451fb62ca0bfb30876cb807413d0675f4c53b8656bbee2d4467cf6434efdcd",
    "zh:a79a206f01caf388261582404fec1ef5ec1c471c753ba0a21c6a9a21ef54bc2f",
    "zh:a7e224e9989f5804346c264dccd8ea4c9147854a08b7e8455e2d2c6def87f341",
    "zh:b5730f80b8a23bab1412415d9d5b6982816bdcabbfe8dd129602c30615581824",
    "zh:c6586fd9e5de1932a8063ea1c25293eba72a9bb5c652c6fd2f06e2bd0723ea0a",
    "zh:d28e4525dc4759b307e20137a4a1e38a4eac1e0ca91d310910afd1f6a08c0946",
  ]
}
